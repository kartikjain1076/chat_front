/* eslint-disable */
import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Login from './login';
import Register from './register';
import Chat from './chat';
import VerifyEmail from './verifyEmail';



const Main = () => (
	<Switch>
	<Route path = '/chat' component = {Chat} />
	<Route path = '/login' component = {Login} />
	<Route path = '/verify/:id' component = {VerifyEmail} />
	<Route exact path = '/' component = {Register} />
	</Switch>
	)


export default Main;