/*eslint-disable*/
import React, {Component} from 'react';
import socketconnection from './socket';
import {userConnect,userConnected,userDisconnected,sendMessage,getMessage,sendRead,sentRead,createGroup,groupCreated,disconnectIt} from './socket';

let flag = {groupName : false, groupUsers : false, message : false};

export default class Chat extends Component{


    constructor(props){
        super(props);
        this.messageRef = React.createRef();
        this.state = {
            userList : [],
            load : true,
            message : '',
            to : null,
            from : localStorage.username,
            messages : [],
            groupChat : false,
            online : '',
            groupUsers : [localStorage._id],
            groupName : '',
            groups : [],
            hasSubmit : false,
            selectedGroup : '',
            group : false

        }
    }

    

    componentDidMount(){
      if(localStorage.username === undefined)
      {
        this.props.history.push('/login');
      }
      userConnect(localStorage.username);
      this.fetchGroup();
      this.fetch_user();
      this.fetchMessage();
      userConnected(data=>{
        console.log(data);
        this.handleChange();
      });
      userDisconnected(()=>{
        this.handleChange();
      });
      getMessage(()=>{
        sendRead({to : this.state.to , from : localStorage.username});
        this.fetchMessage();
        this.scrollToBottom();
      });
      sentRead(()=>{
        this.fetchMessage();
        this.scrollToBottom();
      });
      groupCreated(() => {
        this.setState({groupName : '',groupUsers : [localStorage._id] , groupChat : false});
        this.fetchGroup();
      })
    }
   

    scrollToBottom() {
      if(this.messageRef.current !== null)
      this.messageRef.current.scrollTop = this.messageRef.current.scrollHeight - this.messageRef.current.clientHeight;
    }


    checkGroupName = () => {
      if(this.state.groupName.length <= 0 && this.state.hasSubmit)
      {
        flag.groupName = false;
        return ('Please enter group name');
      }
      else if(this.state.groupName.length > 0)
        flag.groupName = true;
    }

    checkGroupUsers = () => {
      if(this.state.groupUsers.length <= 1 && this.state.hasSubmit)
      {
        flag.groupUsers = false;
        return ('Please enter group name');
      }
      else if(this.state.groupUsers.length > 1)
        flag.groupUsers = true;
    }


    handleChange = () => {
      this.fetch_user();
      this.fetchMessage();
      }

    sendGroupInfo = () => {
      this.setState({hasSubmit : true});
      if(flag.groupName && flag.groupUsers)
      createGroup({groupName : this.state.groupName, groupUsers : this.state.groupUsers});
    }
      
  

    changeData = (e) => {
        this.setState({[e.target.name] : e.target.value});
    }

    changeReceiver = (value,value2) => {
      this.setState({to : value, group : false, selectedGroup : null});
      if(value2 === 'true')
      this.setState({online : 'online'});
      else
      this.setState({online : value2});
      sendRead({to : value , from : localStorage.username});
      this.scrollToBottom();
    }

    changeReceiver2 = (value,value2) => {
      this.setState({to : value, selectedGroup : value2 , group : true});
      this.scrollToBottom();
    }

    logout = () => {
      localStorage.clear();
      disconnectIt({});
      this.props.history.push('/login');

    }

    showReceiver = () => {
      if(typeof(this.state.to) == 'object')
      return(<div className="chat-header clearfix">
      <div className="chat-about">
      <div className="chat-with">{this.state.selectedGroup}</div>
      </div>
      <i style = {{display : 'flex', justifyContent : 'flex-end', fontSize : '30px'}}>{localStorage.username}&nbsp;<img src = 'logout.png' style = {{height : '30px', width : '30px'}} onClick={this.logout} /></i>
      </div>)
      else if(this.state.to !== null)
      return(<div className="chat-header clearfix">
        <div className="chat-about">
        <div className="chat-with">{this.state.to}</div>
        {this.state.online}
        </div>
        <i style = {{display : 'flex', justifyContent : 'flex-end', fontSize : '30px'}}>{localStorage.username}&nbsp;<img src = 'logout.png' style = {{height : '20px', width : '20px'}} onClick = {this.logout} /></i>
        </div>) 
        
    }

    checkMessage = () => {
      if(this.state.message.length > 0 && /\S/.test(this.state.message) == true)
      flag.message = true;
      else
      flag.message = false;
    }

    sendHandler = () => {
      if(flag.message)
      {
        sendMessage({to:this.state.to, from : localStorage.username, message : this.state.message, group : this.state.group, groupName : this.state.selectedGroup});
        this.setState({message : ''});
        this.fetchMessage();
        this.scrollToBottom();
      }
    }

    showGroupChat = () => {
      this.setState({groupChat : this.state.groupChat ? false : true});
    }

    showNewMessage = (username) => {
      let check = 0;
      this.state.messages.map((message)=>{
        if(message.to[0] === localStorage.username && message.from == username && message.read == false)
        {
          check = check + 1;
        }
      })
      if(check > 0)
      {
        const res = 'New Message ('+check+')';
      return(res);
      }
    }

    showJoin = (username) => {
      const check = this.state.groupUsers.indexOf(username);
      if(check == -1)
      return('');
      else
      return('Joined');
    }

    showGroup = () => {
      if(this.state.groupChat == false)
      return this.state.groups.map((grp)=>{
        const check = grp.groupUsers.indexOf(localStorage._id);
        if(check == -1)       
      return(<div>
          

          </div>)
          else
          return(<div>
            <li className="clearfix" onClick = {() => this.changeReceiver2(grp.groupUsers,grp.groupName)}>
            <div className="about">
                <div className="name"><img src='groupChat.png' style={{height : '20px', width : '20px'}} />&nbsp;&nbsp;<a>{grp.groupName}</a></div>
              </div>
                  </li>

            </div>
          )
          
    
  })
}


    returnGroupChat = () => {
      if(this.state.groupChat)
      {return(<div>           
           <input type = 'text' value = {this.state.groupName} placeholder='Group Name' name = 'groupName' onChange={this.changeData} />
           <div style = {{color : 'red'}}>{this.checkGroupName()}</div>
      {
        this.state.userList.map((user)=>{
          if(user.username !== localStorage.username)
          return(
              <div>
              <li className="clearfix"  onClick={() => this.addGroupUser(user._id)} >
               <div className="about" >
                  <div className="name"><a>{user.username}</a></div>
                </div>
                    </li>
                    <li>                
                    <div style = {{color : 'red'}} >{this.showJoin(user._id)}</div>
                    </li>
                    

              </div>
              
          )
      })}
      <button onClick={this.sendGroupInfo}>Create</button>
      <div style={{color : 'red'}}>{this.checkGroupUsers()}</div>
      </div>
      )
      }
      else
      {return(this.state.userList.map((user)=>{
          if(user.username !== localStorage.username && user.online == 'true')
          return(
              <div>
              <li className="clearfix" onClick = {() => this.changeReceiver(user.username,user.online)}>
               <div className="about">
                  <div className="name"><img src='user.png' style={{height : '20px', width : '20px'}} />&nbsp;&nbsp;<a>{user.username}</a></div>
                   <div className="status">
                   <i className="fa fa-circle online" /> online
                </div>
                {this.showNewMessage(user.username)}
                </div>
                    </li>
                    

              </div>
          )
          else if(user.username !== localStorage.username)
          return(
              <div>
              <li className="clearfix" onClick = {() => this.changeReceiver(user.username,user.online)}>
               <div className="about">
                  <div className="name"><img src='user.png' style={{height : '20px', width : '20px'}} />&nbsp;&nbsp;<a>{user.username}</a></div>
                   <div className="status">
                    {user.online}
                </div>
                {this.showNewMessage(user.username)}
                </div>
                    </li>
                    

                    

              </div>
          )
          
      })
      )
      }
    }


    addGroupUser = (user) => {
      const check = this.state.groupUsers.indexOf(user);
      if(check == -1)
      {
        let tempUsers = this.state.groupUsers;
        tempUsers.push(user);
        this.setState({groupUsers : tempUsers});
      }
      else
      {
        let tempUsers = this.state.groupUsers;
        tempUsers.splice(check,1);
        this.setState({groupUsers : tempUsers});
      }
    }


    fetch_user = () => {
      console.log('id',socketconnection.id);
        let option = {
            headers : {'Accept' : 'application/json','Content-Type':'application/json'},
            method : 'Post',
            body : JSON.stringify({})
          }
          fetch("http://localhost:4000/fetch/fetch_user",option)
          .then((response)=>{
            if(response.status===200)
            {
              response.text().then((action)=>
              {
                console.log(typeof(JSON.parse(action)));
                if(typeof(JSON.parse(action)) == "object")
                {
                    let a = JSON.parse(action);
                    this.setState({userList : a.body});
                    this.setState({load : false});
                }
                else
                {
                    alert('Error');
                }
              })
            }
          })
          .catch((err)=>{  
            console.log('Fetch Error :-S', err);
            if(err)
              this.props.history.push('/error');  
          });
    }

    fetchMessage = () => {
      console.log('id',socketconnection.id);
        let option = {
            headers : {'Accept' : 'application/json','Content-Type':'application/json'},
            method : 'Post',
            body : JSON.stringify({username : localStorage.username,from : this.state.to})
          }
          fetch("http://localhost:4000/fetch/fetchMessage",option)
          .then((response)=>{
            if(response.status===200)
            {
              response.text().then((action)=>
              {
                if(typeof(JSON.parse(action)) == "object")
                {
                    let a = JSON.parse(action);
                    this.setState({messages : a.body});
                    this.setState({load : false});
                }
                else
                {
                    alert('Error');
                }
              })
            }
          })
          .catch((err)=>{  
            console.log('Fetch Error :-S', err);
            if(err)
              this.props.history.push('/error');  
          });
    }

    fetchGroup = () => {
        let option = {
            headers : {'Accept' : 'application/json','Content-Type':'application/json'},
            method : 'Post',
            body : JSON.stringify({})
          }
          fetch("http://localhost:4000/fetch/fetchGroup",option)
          .then((response)=>{
            if(response.status===200)
            {
              response.text().then((action)=>
              {
                if(typeof(JSON.parse(action)) == "object")
                {
                    let a = JSON.parse(action);
                    this.setState({groups : a.body});
                }
                else
                {
                    alert('Error');
                }
              })
            }
          })
          .catch((err)=>{  
            console.log('Fetch Error :-S', err);
            if(err)
              this.props.history.push('/error');  
          });
    }

    showSendArea = () => {
      if(this.state.to !== null)
      return(
      <div className="chat-message clearfix">
      <textarea name="message" id="message-to-send" placeholder="Type your message" value = {this.state.message} rows={3} onChange = {this.changeData} />
      <i className="fa fa-file-o" /> &nbsp;&nbsp;&nbsp;
      <i className="fa fa-file-image-o" />
      <button onClick={this.sendHandler}>Send</button>
      {this.checkMessage()}
    </div>
    )
  }


    render(){
        if(this.state.load)
        return(<div></div>);
        else
        return(
            <div>
 
  <div className="container clearfix">
    <div className="people-list" id="people-list">
      <div className="search" onClick={this.showGroupChat}>
        <img src = 'groupChat.png' style = {{height : '35px', width : '30px', position : 'absolute'}}  />
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create Group
        </div>
      <ul className="list">

            {this.returnGroupChat()}
            {this.showGroup()}

            
        

        
      </ul>
    </div>
    <div className="chat">

    {this.showReceiver()}


    <div className="chat-history" ref={this.messageRef}>
        <ul>

            {
              this.state.messages.map((message)=>{
                if(message.group)
                {
                  if(message.groupName == this.state.selectedGroup)
                  {
                    if(message.from == localStorage.username)
                    return(<li className="clearfix">
                    <div className="message-data align-right">
                    <span class="message-data-time" >{message.time}</span> &nbsp; &nbsp;
                      <span className="message-data-name">{message.from}</span> <i className="fa fa-circle me" />
                    </div>
                    <div className="message other-message float-right" style = {{ 'word-wrap' : 'break-word'}}>
                      {message.message}
                    </div>
                  </li>);
                    else
                    return(<li>
                      <div className="message-data">
                      <span class="message-data-time" >{message.time}</span> &nbsp; &nbsp;
                        <span className="message-data-name"><i className="fa fa-circle online" /> {message.from}</span>
                      </div>
                      <div className="message my-message" style = {{ 'word-wrap' : 'break-word'}}>
                        {message.message}
                      </div>
                    </li>);
                  }
                }
                else if(message.from == localStorage.username && this.state.to == message.to)
                {
                  if(message.delivered && message.read)
                  return(
                    <li className="clearfix">
                <div className="message-data align-right">
                <span class="message-data-time" >{message.time}</span> &nbsp; &nbsp;
                  <span className="message-data-name">{message.from}</span> <i className="fa fa-circle me" />
                </div>
                <div className="message other-message float-right" style = {{ 'word-wrap' : 'break-word'}}>
                  {message.message}
                </div>
                <img src='bluetick.png' style={{width : '20px', height:'20px'}} /> 
              </li>)
              else if(message.delivered)
              return(
                <li className="clearfix">
            <div className="message-data align-right">
            <span class="message-data-time" >{message.time}</span> &nbsp; &nbsp;
              <span className="message-data-name">{message.from}</span> <i className="fa fa-circle me" />
            </div>
            <div className="message other-message float-right" style = {{ 'word-wrap' : 'break-word'}}>
              {message.message}
            </div>
            <img src='doublegreentick.png' style={{width : '20px', height:'20px'}} /> 
          </li>)
          else
          return(
            <li className="clearfix">
        <div className="message-data align-right">
        <span class="message-data-time" >{message.time}</span> &nbsp; &nbsp;
          <span className="message-data-name">{message.from}</span> <i className="fa fa-circle me" />
        </div>
        <div className="message other-message float-right" style = {{ 'word-wrap' : 'break-word'}}>
          {message.message}
        </div>
        <img src='greentick.png' style={{width : '20px', height:'20px'}} /> 
      </li>)
                }
            else if(message.from == this.state.to && message.to == localStorage.username)
          return(
            <li>
            <div className="message-data">
            <span class="message-data-time" >{message.time}</span> &nbsp; &nbsp;
              <span className="message-data-name"><i className="fa fa-circle online" /> {message.from}</span>
            </div>
            <div className="message my-message" style = {{ 'word-wrap' : 'break-word'}}>
              {message.message}
            </div>
          </li>
          )
          else if(this.state.to == null)
          return(<div className='message-data'></div>)

          })
          
          }



        </ul>



      </div>
       
      {this.showSendArea()}
    </div>
  </div>
</div>

        )
    }
}