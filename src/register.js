/*eslint-disable*/
import React, { Component } from 'react';
import {Link} from 'react-router-dom';

let flag = {name : false, username : false, password : false, email : false};

class Register extends Component {
  constructor(props){
    super(props);
    this.state = {
      name : '',
      username : '',
      password : '',
      email : '',
      hasSubmit : false
    };
  }

  checkData = () => {
    if(this.state.name.length > 0 && this.state.username.length > 0 && this.state.password.length > 0 && this.state.email.length > 0)
    {
      flag.name = true;
      flag.username = true;
      flag.password = true;
      flag.email = true;
    }
    else if(this.state.hasSubmit)
    {
      flag.name = false;
      flag.username = false;
      flag.password = false;
      flag.email = false;
      return('Please enter the value');
    }
    else
    {
      flag.name = false;
      flag.username = false;
      flag.password = false;
      flag.email = false;
    }
  }

  componentDidMount = () => {
    if(localStorage.username !== undefined)
    {
      this.props.history.push('/chat');
    }
  }

  changeData = async(e) => {
    await this.setState({[e.target.name] : e.target.value});
  }

  submitHandler = (e) => {
    e.preventDefault();
    this.setState({hasSubmit : true});
   if(flag.username && flag.name && flag.password && flag.email){
      let option = {
      headers : {'Accept' : 'application/json','Content-Type':'application/json'},
      method : 'Post',
      body : JSON.stringify(this.state)
    }
    fetch("http://localhost:4000/register",option)
    .then((response)=>{
      if(response.status===200)
      {
        response.text().then((action)=>
        {
          console.log(action);
          if(action === 'ok')
          {
            this.setState({name : '', username : '', password : '', email : ''});
            this.props.history.push('/login');
          }
          else
          {
            console.log('err');
          }
        })
      }
    })
    .catch((err)=>{  
      console.log('Fetch Error :-S', err);
      if(err)
        this.props.history.push('/error');  
    });
  }
  }

  render() {
    return (
      <div className="App">
          
      <link rel="stylesheet" href="css/style.css" />
      
      <body>
      
        
      <div class="panel-lite">
        <div class="thumbur">
          <div class="icon-lock"></div>
        </div>
        <h4>Register</h4>
        <div class="form-group">
          <input class="form-control" name = 'name' required="required" onChange={this.changeData} />
          <label class="form-label">Name    </label>
          <div style={{color : 'red'}}>{this.checkData()}</div>
        </div>
        <div class="form-group">
        <input class="form-control" name='username' required="required"onChange={this.changeData} />
        <label class="form-label">Username    </label>
        <div style={{color : 'red'}}>{this.checkData()}</div>
        </div>
      <div class="form-group">
      <input class="form-control" name = 'email' required="required"onChange={this.changeData} />
      <label class="form-label">Email    </label>
      <div style={{color : 'red'}}>{this.checkData()}</div>
      </div>
        <div class="form-group">
          <input class="form-control" type="password" name = 'password' required="required" onChange={this.changeData} />
          <label class="form-label">Password</label>
          <div style={{color : 'red'}}>{this.checkData()}</div>
        </div><a href="#"><Link to = '/login'>Login</Link>  </a>
        <button class="floating-btn" onClick={this.submitHandler}><i class="icon-arrow"></i></button>
      </div>
        
        
      
      </body>
      </div>
    );
  }
}

export default Register;
