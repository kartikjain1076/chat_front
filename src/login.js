/*eslint-disable*/
import React, { Component } from 'react';
import {Link} from 'react-router-dom';


class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      username : '',
      password : ''
    }
  }

  changeData = (e) => {
    this.setState({[e.target.name] : e.target.value});
  }

  componentDidMount = () => {
    if(localStorage.username !== undefined)
    this.props.history.push('/chat');
  }

  submitHandler = (e) => {
    console.log('sdf');
    e.preventDefault();
    let option = {
      headers : {'Accept' : 'application/json','Content-Type':'application/json'},
      method : 'Post',
      body : JSON.stringify(this.state)
    }
    fetch("http://localhost:4000/login",option)
    .then((response)=>{
      console.log(response);
      if(response.status===200)
      {
        response.text().then((action)=>
        {
          console.log(action);
          if(typeof(JSON.parse(action)) == "object")
                {
                    let a = JSON.parse(action);
                    if(a.res === 'ok')
                    {
                      if(a.body[0].verified)
                      {
                        localStorage.username = a.body[0].username;
                        localStorage.password = a.body[0].password;
                        localStorage._id = a.body[0]._id;
                        this.props.history.push('/chat');
                      }
                      else
                      {
                        alert('Please verify your E-Mail Id');
                      }
                    }
                    else
                    {
                      alert('Incorrect username or password');
                    }
                }
          else
          {
            console.log('err');
          }
        })
      }
    })
    .catch((err)=>{  
      console.log('Fetch Error :-S', err);
      if(err)
        this.props.history.push('/error');  
    });
  }


  render() {
    return (
      <div className="App">
          
      <link rel="stylesheet" href="css/style.css" />
      
      <body>
      
        
      <div class="panel-lite">
        <div class="thumbur">
          <div class="icon-lock"></div>
        </div>
        <h4>Login</h4>
        
        <div class="form-group">
        <input class="form-control" name = 'username' required="required" onChange={this.changeData} />
        <label class="form-label">Username    </label>
      </div>
      
        <div class="form-group">
          <input class="form-control" name = 'password' type="password" required="required" onChange={this.changeData} />
          <label class="form-label">Password</label>
        </div><a href="#"><Link to ='/'>Register</Link>  </a>
        <button class="floating-btn" onClick={this.submitHandler}><i class="icon-arrow"></i></button>
      </div>
        
        
      
      </body>
      </div>
    );
  }
}

export default Login;
